# CHANGELOG

<!--- next entry here -->

## 0.2.0
2020-08-19

### Features

- Update terms.md (10b19345eff66e869572a1ac7f98e64f89405efd)

## 0.1.0
2020-08-19

### Features

- Update terms.md (0bd679f122212268092a56c166fa0119232e9eb6)

### Fixes

- New section (5700eb4d0cee20cb32f47717238544194826705a)

## 0.1.0
2020-03-11

### Features

- Update terms.md (0bd679f122212268092a56c166fa0119232e9eb6)

## 0.0.1
2020-03-11

### Fixes

- New section (5700eb4d0cee20cb32f47717238544194826705a)

## 0.0.1
2020-03-11

### Fixes

- New section (5700eb4d0cee20cb32f47717238544194826705a)