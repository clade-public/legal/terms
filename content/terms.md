# End User License Agreement (“EULA”) 

## 1. Scope

This EULA provides the terms and conditions under which micro-biolytics GmbH, Schelztorstrasse 54, 73728 Esslingen am Neckar (hereinafter „MBIO“) provides respectively licenses its hardware and software product to you. MBIO’s hardware and software product consists of the Mira Analyzer and the underlying Aqua Spec Technology (together hereinafter „Hardware“), the software running on the Hardware as well as the Software Applications provided by MBIO via the MBiosphere Cloud (together hereinafter „Software“).

## 2.License
  
* **2.1**    MBIO grants you a worldwide, yet limited, that is to say non-exclusive, non-transferable and non-sublicensable right to use the Hardware and Software for the term of the contractual relationship between MBIO and your company and exclusively for the business purposes of your company. 

* **2.2**    You are not entitled to rent out, lease or sublicense the Hardware and/or Software and/or to copy, distribute, sell and/or make the Hardware and/or Software available to third parties. Furthermore, you are not entitled to reverse-engineer or reverse-translate the Software or to remove program parts. 

* **2.3**    You are not entitled to remove identifiers, trademarks and/or copyright notices from the Hardware and/or Software, make changes to or manipulate the Hardware and/or Software or request the source code to the Software.

* **2.4**    The mandatory provisions of §§ 69 a ff. UrhG (German Act on Copyright and Related Rights) remains unaffected.

* **2.5**    When using the Hardware and Software, you undertake not to violate applicable law and not to infringe third party rights.

* **2.6**    Unless expressly provided in this Section 2, all industrial property rights and copyrights as well as all associated rights of use in and to the Hardware and Software fully lie and remain with MBIO.

## 3. Modifications
* **3.1**    MBIO reserves all rights to make changes to the Hardware and/or Software and/or to provide updates and upgrades (together hereinafter referred to as “Modifications”).

* **3.2**    All provisions regarding modifications are exclusively dealt with in the contractual relationship between MBIO and your company. You are not entitled to request certain modifications.

* **3.3**    MBIO will inform you about any Modifications in due course.  

## 4. Liability
* **4.1**    MBIO shall be liable without limitation for intentional wrongdoing and gross negligence as well as in case of injury to life, body or health.

* **4.2**    With regard to ordinary negligence, MBIO shall only be liable if essential contractual obligations are breached. Essential contractual obligations are obligations the fulfilment of which is essential for the proper performance of the agreement and the observance of which the contractual partner may rely on. In these cases, MBIO’s liability is limited to any direct, contract-typical and foreseeable damages.

* **4.3**    Any liability of MBIO for indirect and unforeseeable damage, loss of data, data network failures, loss of production and use, loss of turnover, loss of profit, loss of savings and/or financial loss due to claims by third parties shall be excluded in the event of ordinary negligence.

* **4.4**    The limitations or exclusions of liability pursuant to Sections 4.2 and 4.3 shall not apply in case of statutory strict liability (e.g. under the German Product Liability Act) and/or liability arising from a strict guarantee.

* **4.5**    MBIO shall not be liable for direct and/or indirect damages to the degree your use of the Hardware and/or Software violates this EULA and/or requirements of usage for the Hardware and/or Software that MBIO has duly communicated, including: 

    the Hardware and/or Software may not be used in a network configuration or network infrastructure not authorized by MBIO;
    the Hardware and/or Software may not be used in connection with hardware and/or software not authorized by MBIO for this purpose;
    modifications, repairs and/or other interventions in and to the Hardware and/or Software may not be performed by unauthorized service providers or individuals.

## 5. Third Party Rights
* **5.1**    In case a third party asserts claims against you in connection with your usage of the Hardware and/or Software, MBIO shall indemnify you against such claims.

* **5.2**    MBIO’s indemnification obligation pursuant to Section 5.1 does not apply to the degree your usage of the Hardware and/or Software violates this EULA and/or in case you knowingly or negligently violate applicable law or the rights of third parties.

* **5.3**    MBIO’s indemnification obligation pursuant to Section 5.1 requires that you immediately ask your company to inform MBIO of the claims raised by a third party, leave the defense of rights to MBIO depending on MBIO's decision and support MBIO to a reasonable extent free of charge.

## 6. Miscellaneous

* **6.1**    Upon termination of the contractual relationship between MBIO and you company, you are obligated to immediately stop using the Hardware and/or Software. MBIO may block you access to the Hardware and/or Software correspondingly. 

* **6.2**    Amendments and supplements to this EULA must be made in writing. This shall also apply to any waiver of this written form clause.

* **6.3**    Should a provision of this EULA be or become invalid, the validity of the remaining provisions shall remain unaffected. 

* **6.4**    This Agreement shall be governed exclusively by the laws of the Federal Republic of Germany to the exclusion of conflict of laws’ provisions.